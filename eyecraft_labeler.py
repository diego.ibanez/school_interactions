
import pandas as pd
import cv2
from PIL import Image
import sys
import numpy as np


def main(label_name, video_path, detections_path, n_sample=2):

    video_capture = cv2.VideoCapture(video_path)

    frame_df = pd.read_csv(detections_path, sep=",")
    frame_df = frame_df[(frame_df['face_tl_y'] != frame_df['face_br_y'])]
    frame_df = frame_df[(frame_df['face_tl_x'] != frame_df['face_br_x'])]
 
    ### GETTING ALL THE DATA
    faces_sample ={} 
    for name, df in frame_df.groupby(['id_subject']):
        if df.shape[0]<n_sample:
            continue
        faces_sample.update({name: df.set_index('id_frame').sample(n=n_sample).to_dict(orient='index')})

    ### PROCESSING
    results = {}
    for sample_subject, sample_dict in faces_sample.items():
        aux = {}    
        for frame_no, face_crop in sample_dict.items():
            video_capture.set(cv2.CAP_PROP_POS_FRAMES,int(frame_no))
            ret, frame = video_capture.read()
            crpim = np.copy(frame[face_crop['face_tl_y']:face_crop['face_br_y'], face_crop['face_tl_x']:face_crop['mouth_br_x']])


            cv2.imshow('face',crpim)           
            class_ = input(label_name)

            cv2.waitKey(0)

            aux.update({frame_no:{'class':class_,'image':crpim}})


        results.update({sample_subject: mode(list(aux.values()))})
    
    n = 0
    with open('data/finetune_images/finetune_images.txt', 'w+') as filetxt:
      for s, dict_ in results.items():
        for f, dict_f in dict_.items():
            cv2.imwrite('data/finetune_images/{}/{}_{}.jpg'.format(label_name,dict_f['class'],str(n)), crpim)
            n += 1
            filetext.write('{} {}'.format(dict_f['class'],str(n)))

    
    
    return pd.DataFrame.from_dict(results, orient='index', columns=[label_name])




if __name__ == '__main__':

    label_name = sys.argv[1]
    video_path = sys.argv[2]
    detections_path = sys.argv[3]
    save_path = detections_path + '.eyelabelerresult'

    df = main(label_name, video_path, detections_path)

    df.to_csv(save_path, index_label='subject_id')