import pandas as pd
import sys
import missingno as msno
from tracker import tracker



def main(path_video, path_to_save):
    
    #exect tracker
    tracker.main(path_video=path_video, path_to_save=path_to_save)

    df = make_pivot_dataframe(path_to_save)

    df.to_csv(path_to_save+'.pivoted')
    make_fig_null_analysis(df, path_to_save)

    return True    


def make_pivot_dataframe(path):
    df = pd.read_csv(path,sep=',')
    
    def func(r):
        return [r[0],r[1],list(r[2:])]

    df_ = df.apply(func, axis=1).apply(pd.Series)
    
    df_f = df_.pivot(index=0,columns=1,values=2)
    
    return df_f

def make_fig_null_analysis(df,path_to_save):
    fig = msno.matrix(df)
    fig_copy = fig.get_figure()
    fig_copy.savefig(path_to_save+'.matrix-nan.png')
    fig = msno.bar(df)
    fig_copy = fig.get_figure()
    fig_copy.savefig(path_to_save+'.bar-nan.png')
    return True


if __name__ == '__main__':
    if len(sys.argv) >1:
        w = main(path_video=sys.argv[1], path_to_save=sys.argv[2])
    else:
        w = main(path_video=None)
    
    if w == True:
        print('DONE!')
    else:
        print('ERROR!')