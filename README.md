
### INSTALLING



1) create a virtualenv with Python 3.6 and do:
	>pip install -r requirements.txt

2) initiate the submodule traker:
	>git submodule init
	>git submodule update
	
3) download yolo.h5 from https://drive.google.com/file/d/1uvXFacPnrSMw6ldWTyLLjGLETlEsUvcE to folder tracker/deep_sort_yolov3/model_data/ as the deep_sort_yolov3 project README set

## school network analysis, bases in social interaction in educational enviroments

1) Aparitions retriver:
    >python apparitions_retriver.py path_to_video path_to_save_aparitions

result: file.csv with aparitions + file.csv.avi with the video of apartitions 


2) Age/Gender predictor:
    >python age_gender_predictor.py path_to_aparitions_results path_to_video n_samples_to_use"

result: file.csv with subject, age, gender

3) activity voice detection:
    >python activity_voice_detection.py path_to_aparitions_results path_to_video window_size n_subvideos"

result: file.csv with the subjects as columns, and the frames as index. The values "1": if detect activity voice. "0" if not.


