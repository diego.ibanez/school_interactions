import pandas as pd
import cv2
from PIL import Image
import sys
import numpy as np
from statistics import mode 
from collections import Counter


## Arguments parcered

## 1: path to detection file
path_results_detection = sys.argv[1]
## 2: path to video  
path_video = sys.argv[2]
## 3: path to save detection
path_to_save = path_results_detection + '.age_gender_detection'
## 4: samples to use for the detection
sample_n = int(sys.argv[3])

##### Age predict config

# define the list of age buckets our age detector will predict
AGE_BUCKETS = ["(0-2)", "(4-6)", "(8-12)", "(15-20)", "(25-32)","(38-43)", "(48-53)", "(60-100)"]
# load our serialized age detector model from disk
#print("[INFO] loading age detector model...")
ageProto ='gender_age_data_model/age_deploy.prototxt'
ageModel = 'gender_age_data_model/age_net.caffemodel'

ageNet = cv2.dnn.readNet(ageProto, ageModel)

##### Gender predict config

genderModel="gender_age_data_model/gender_net.caffemodel"
genderProto="gender_age_data_model/gender_deploy.prototxt"

MODEL_MEAN_VALUES=(78.4263377603, 87.7689143744, 114.895847746)
genderList=['Male','Female']
genderNet=cv2.dnn.readNet(genderModel,genderProto)


## LOADING VIDEO CAPTURA
video_capture = cv2.VideoCapture(path_video)

## LOADING DETECTIONS RESULT FRAMES
frame_df = pd.read_csv(path_results_detection, sep=",")
frame_df = frame_df[(frame_df['face_tl_y'] != frame_df['face_br_y'])]
frame_df = frame_df[(frame_df['face_tl_x'] != frame_df['face_br_x'])]


## CREATE SAMPLES
samples ={}
for name, df in frame_df.groupby('id_subject'):
    df_sample = df.sample(n=sample_n, replace=True).drop_duplicates(subset=None, keep='first', inplace=False).set_index('id_frame').to_dict(orient='index')
    samples.update({name:df_sample})

## INITIALIZATION RESULTS DICT
results = {}

## ITERATION OVER ALL THE SUBJECTS
for sample_subject, sample_dict in samples.items():
    ## Aux lists
    gender_aux = []
    age_aux = []
    ## ITERATION OVER ALL SAMPLES OF THE SUBJECT
    for frame_no, face_crop in sample_dict.items():
        video_capture.set(cv2.CAP_PROP_POS_FRAMES,int(frame_no))
        ret, frame = video_capture.read()

        ### GENDER IDENTIFICATION
        crpim = np.copy(frame[face_crop['face_tl_y']:face_crop['face_br_y'], face_crop['face_tl_x']:face_crop['face_br_x']])
        blob= cv2.dnn.blobFromImage(crpim, 1.0, (227,227), MODEL_MEAN_VALUES, swapRB=True)
        #cv2.imshow('', blob)
        #cv2.waitKey(0)
        genderNet.setInput(blob)
        genderPreds=genderNet.forward()
        gender_Confidence = (int(genderPreds[0][1]*100)/100)
        gender_aux.append(gender_Confidence)

        ### AGE PREDICTION
        # make predictions on the age and find the age bucket with
		# the largest corresponding probability
        ageNet.setInput(blob)
        agePreds = ageNet.forward()
        age_class = agePreds[0].argmax()
		#ageCo = int(agePreds[0][i]*100)/100
        age_aux.append(age_class)#ageConfidence)
    
    try:
        mode_age = mode(age_aux)
    except:
        mode_age = round(np.mean(age_aux))
    ### UPDATE RESULTS FOR EACH SUBJECT
    results.update({sample_subject: {'n_sample': len(sample_dict)  ,'mean_gender_pred': sum(gender_aux)/len(gender_aux),'mode_age_class': mode_age}})


## dict of results to dataframe
df_results = pd.DataFrame.from_dict(results, orient='index', columns=['mean_gender_pred','mode_age_class','n_sample'])

## naming of the result
df_results['gender'] = [genderList[round(value)] for value in df_results['mean_gender_pred']]
df_results['age_interval'] = [AGE_BUCKETS[round(value)] for value in df_results['mode_age_class']]

## saving resulta table
df_results.to_csv(path_to_save, index_label='subject')





        
