import pandas as pd
import cv2
from PIL import Image
import sys
import numpy as np
from scipy.stats import chi2, norm
import itertools #importing the module
#combined_list1 = itertools.chain(list1 ,list2, list3)

def make_windows(secuence_dict, n_window):
    windows = {}

    frames = list(secuence_dict.keys())
    
    for n, f in enumerate(frames):
       start = 0 if n-int(n_window/2)<0 else n-int(n_window/2)
       end = len(frames) if n+int(n_window/2) > len(frames) else n+int(n_window/2)
       windows.update({f:[secuence_dict[v] for v in frames[start:end+1]]})    
    #for frame, value in secuence_dict.items():
    #    windows.update({frame+int(n_window/2):[secuence_dict[v] for v in range(frame,frame+n_window) if v in list(secuence_dict.keys())]})    
    return windows

def visual_voice_detection(path_results_detection,path_video, window_size, PFA=0.01, porc=0.1, n_subvids=50):
    video_capture = cv2.VideoCapture(path_video)      
    
    ### MAKING FRAMES {frame_no :{id_ : (w,h)} }  
    frame_df = pd.read_csv(path_results_detection, sep=",")
    print('###')
    frame_df = frame_df[(frame_df['mouth_tl_y'] != frame_df['mouth_br_y'])]
    frame_df = frame_df[(frame_df['mouth_tl_x'] != frame_df['mouth_br_x'])] 

    ## GETTING ALL THE DATA
    subject_data_dict = { name : df.set_index('id_frame').to_dict(orient='index') for name, df in frame_df.groupby(['id_subject']) } 

    ## GATHERING THE SIGNALS PEAR SUBJECT
    results = {}
    for subject_name, subject_data in subject_data_dict.items():
        aux = {}    
        frames = list(subject_data.keys())
        l_subvids = int(len(frames)/n_subvids)
        sub_videos_frames = [frames[x:x+l_subvids] for x in range(0, len(frames)-1,  l_subvids)]
        sub_videos = [{f:subject_data[f] for f in sv} for sv in sub_videos_frames]
        full_voice_detection = {}
        for sv_n, sub_v in enumerate(sub_videos):
            print(subject_name, sv_n)
            
            if len(sub_v) == 0:
                continue

            video_capture.set(cv2.CAP_PROP_POS_FRAMES,int(list(sub_v.keys())[2])) ##use the 3th frame
            ret, frame = video_capture.read()
            face_crop = sub_v[list(sub_v.keys())[0]]
            crpim = np.copy(frame[face_crop['mouth_tl_y']:face_crop['mouth_br_y'],face_crop['mouth_tl_x']:face_crop['mouth_br_x']])
            gray = cv2.cvtColor(crpim, cv2.COLOR_BGR2GRAY)
            
            ## Adjusting T: treashold of noise
            T = np.mean(gray)/2
            
            gray_list_pix = []
            for l in gray.tolist():
                gray_list_pix.extend(l)

            while len([p for p in gray_list_pix if p < T]) == 0:
                T = T*1.05 ## up just a little
                print(T)            
            
            x_n = {}

            ## FIRST X_n 
            for frame_no, face_crops in sub_v.items():
                video_capture.set(cv2.CAP_PROP_POS_FRAMES,int(frame_no))
                ret, frame = video_capture.read()
                
                crpim = np.copy(frame[face_crops['mouth_tl_y']:face_crops['mouth_br_y'],face_crops['mouth_tl_x']:face_crops['mouth_br_x']])
                
                try:
                   cv2.imshow('MOUTH',crpim)
                   cv2.waitKey(0)
                except:
                    pass
                #print(crpim)
                try:
                   gray = cv2.cvtColor(crpim, cv2.COLOR_BGR2GRAY)
                except:
                   print(crpim)
                   continue
                #gray_list_pix = list(itertools.chain(gray.tolist()))
                #for l in gray.tolist():
                #    gray_list_pix.extend(l)
                gray_list_pix = []
                [gray_list_pix.extend(el) for el in gray.tolist()] 
                #n_undertreshold = len([p for p in gray_list_pix if p < T])

                df_aux = pd.DataFrame({'pix':gray_list_pix})
                n_undertreshold = sum(df_aux['pix'].apply(lambda x: True if x < T else False ))

                pix_n = len(gray_list_pix)
                rate_pix_under = n_undertreshold/pix_n
                print(n_undertreshold, pix_n, rate_pix_under)
                
                x_n.update({frame_no: rate_pix_under})
                
                #print(gray, T)
                #print(np.where(gray< T), len(np.where(gray< T)))
            
            ## FIRST s0 with 10% lower 
            x_n_lower10 = sorted(list(x_n.values()))[:int(len(x_n)/10)]
            mean_lower10 = np.mean(x_n_lower10)
            s0 = np.std([xn-mean_lower10 for xn in x_n_lower10])

            #print(s0)
            ## CREATING WINDOWS
            windows = make_windows(x_n, window_size)
            #print(windows)
            ## ADJUSTING s0 & creating full detection
            vuelta = 0
            s1 = 0
            while True:
                vuelta = vuelta + 1
                voice_detection = {}
                for frame, window in windows.items():
                    d_f = len(window)
                    #print(d_f)
                    tt1 = np.sqrt(s0*s0/len(window))*norm.ppf(1-PFA)  
                    tt2 = s0*s0*chi2.ppf(1-PFA, d_f)
                    
                    T1 = np.mean(window)
                    T2 = np.sum(np.square(window))
                    
                    #print(tt1,tt2)
                    #print(s0, window)
                    #pint(T1,T2)
                    
                    detection = 0
                    if T1 > tt1:
                        if T2 > tt2:
                            detection = 1
                        
                    #detection = 1 if ((T1 > tt1) & (T2 > tt2)) else 0
                    print(frame,detection)

                    s_abs = np.absolute(s0-s1)
                    print(s0,s1, s_abs, vuelta)

                    voice_detection.update({frame: detection}) 
                
                
                X_no_speaking = [ x_n[f] for f, d in voice_detection.items() if d == 0 ]
                mean_speaking = np.mean(X_no_speaking)
                
                s1 = np.std([xn-mean_lower10 for xn in X_no_speaking])

                s_abs = np.absolute(s0-s1)
                
                print(s0,s1, s_abs, vuelta)
                if s_abs <= 0.01:
                    break
                else:
                    s0 = s1

            ## UPDATE DE VOICE DETECTION BY FRAME
            full_voice_detection.update(voice_detection)
        
        ## UPDATE DE RESULTS BY SUBJECT
        results.update({subject_name: full_voice_detection})


    ## CREATION OF DATAFRAME OF DETECTIONS    
    df_result = pd.DataFrame(index=range(0,frame_df['id_frame'].max()))
    for subject, detections in results.items():
           df = pd.DataFrame.from_dict(detections, orient='index', columns=[subject]).sort_index()
           df_result= df_result.merge(df, left_index= True, right_index=True, how='left')
    df_result.fillna(0, inplace=True)
    
    return df_result.astype('int')

if __name__ == "__main__":
    path_results_detection = sys.argv[1]
    path_video = sys.argv[2]
    
    window_size = int(sys.argv[3])
    n_subvids = int(sys.argv[4])
    
    path_to_save = path_results_detection + '.voice_detection' #sys.argv[3]
    
    df_result = visual_voice_detection(path_results_detection=path_results_detection,path_video=path_video,window_size=window_size, n_subvids=n_subvids)
    df_result.to_csv(path_to_save, index_label='frame')