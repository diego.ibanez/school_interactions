import pandas as pd
from datetime import datetime
import sys
import numpy as np

def main(path, n):
    df = pd.read_csv(path, sep=",")
    #df.index = df['0'].tolist()
    df.drop('0',axis=1, inplace=True)
    #print(df)
    dict_intervals = {}
    for c in df.columns:
        index_list = df[df[c].isnull() == False].index.tolist()
        if len(index_list) < 90:
            continue
        start = index_list[0]
        end = index_list[-1]
        #print(end)
        #print(df[df[c].isnull() == False])
        #print(df[c].iloc[start])
        #print(df[c].iloc[end])
        if c in ['10','160']:
            print(c, start, end)
        dict_intervals.update({c:{'start': start, 'end': end}})

    candidates_dict = {}
    for c, d in dict_intervals.items():
        aux = []
        for c_, d_ in dict_intervals.items():
            if c != c_:
                dif = d_['start'] - d['end']
                if  (abs(dif) <= n):# & (dif > 0) :
                    #if (c == '1') & (c_ == '81'):
                    #    print(dif)
                    aux.append(c_)

        candidates_dict.update({c:aux})

    NEXT_DICT= {}
    for c, lc in candidates_dict.items():
      #if c != '0':  
        aux = []
        list_crops_end = df[c].iloc[dict_intervals[c]['end']].strip('][').split(', ')

        for c_ in lc:
            r = []
            list_crops_start = df[c_].iloc[dict_intervals[c_]['start']].strip('][').split(', ')
         #   print(list_crops_start)
            for n, l in enumerate(list_crops_start):
                r.append((int(l)-int(list_crops_end[n]))**2)
            aux.append(np.sqrt(sum(r[:2])))

        NEXT_DICT.update({c: lc[np.argmin(aux)] if len(aux)>0 else ''})
    
    df = pd.DataFrame({'init': list(NEXT_DICT.keys()),'next': list(NEXT_DICT.values())})
    #print(df)
    df = df[df['init'].astype(int)<df['next'].astype(int)]
    print(df)
    return df

if __name__ == '__main__':

    start = datetime.now()
    pivoted_path = sys.argv[1]
    n = int(sys.argv[2])
    df = main(pivoted_path, n)
    df.to_csv(pivoted_path+'.align_list_{}.csv'.format(n), index=False)
    print("DONE!")
    finish = datetime.now()
    print(finish-start)