#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
from timeit import time
import warnings
import sys
#sys.path.append(".")
import cv2
import numpy as np
from PIL import Image
from .deep_sort_yolov3.yolo import YOLO

from .deep_sort_yolov3.deep_sort import preprocessing
from .deep_sort_yolov3.deep_sort import nn_matching
from .deep_sort_yolov3.deep_sort.detection import Detection
from .deep_sort_yolov3.deep_sort.tracker import Tracker
from .deep_sort_yolov3.tools import generate_detections as gdet
from .deep_sort_yolov3.deep_sort.detection import Detection as ddet

import mtcnn
from mtcnn.mtcnn import MTCNN

warnings.filterwarnings('ignore')

#import sys
sys.path.append(".")

def main(yolo = YOLO(), path_video = None,nms_max_overlap= 1.0, path_to_save= 'detections.csv', Video_view=False, print_fps= False, writeVideo_flag=True, fps = 30):

    # Deep_sort load person filter
    model_filename = './tracker/deep_sort_yolov3/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename,batch_size=1)


    # Definition of the parameters to Tracker
    max_cosine_distance = 0.3
    nn_budget = None
   # nms_max_overlap = 1.0

    ## Init metric Tracker
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)

    ## Init Tracker
    tracker = Tracker(metric)

    ## Video from path
    video_capture = cv2.VideoCapture(path_video)

    # Define the codec and create VideoWriter object
    w = int(video_capture.get(3))
    h = int(video_capture.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')#'MJPG')
    
    ## File to save video output
    if writeVideo_flag:
        out = cv2.VideoWriter(path_to_save+'.output.mp4', fourcc, fps, (w, h))

    ## File to save detections
    
    list_file = open(path_to_save, 'w')
    
    
    list_file.write('id_frame,id_subject,subject_tl_x,subject_tl_y,subject_br_x,')
    list_file.write('subject_br_y,face_tl_x,face_tl_y,face_br_x,face_br_y,mouth_tl_x,mouth_tl_y,mouth_br_x,mouth_br_y')
                         
    list_file.write('\n')
    ## face, mouth
    detector = MTCNN()

    ## frame init
    frame_index = -1
    ## fps init
    fps = 0.0
    while True:
        ret, frame = video_capture.read()  # frame shape 640*480*3
        if ret != True:
            #print(ret)
            #print(frame)
            break
        t1 = time.time()

        image = Image.fromarray(frame[...,::-1]) #bgr to rgb
        boxs = yolo.detect_image(image) # yolos answer   
        features = encoder(frame,boxs) # vectors encoded from the boxes of frames
    
        # Score to 1.0 here
        detections = [Detection(bbox, 1.0, feature) for bbox, feature in zip(boxs, features)]
        
        # Run non-maxima suppression
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]
        
        # Call the tracker
        tracker.predict()
        tracker.update(detections)
        
        frame_index = frame_index +1
        
        
        
        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:  ##time to retrack
                continue
                #break
            bbox = track.to_tlbr()
            #print(frame_index)
          #  crpim = frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x].copy()

            
           # cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 2)
           
           # track_frame = frame[int(bbox[1]):int(bbox[1])+int(bbox[3]), int(bbox[0]):int(bbox[0])+int(bbox[2])].copy()
            track_frame = frame[int(bbox[1]):int(bbox[3]), int(bbox[0]):int(bbox[2])].copy()
            if len(track_frame) > 0:
                try:
                  #gray = cv2.cvtColor(track_frame, cv2.COLOR_BGR2GRAY)
                  faces = detector.detect_faces(track_frame)
                  #print(frame_index)
                except:
                  if track_frame[1] == 0:
                      print(True)
                  faces = []
            else:
                faces = []

            szs = []
            for f in faces:
                x,y,h,w = f['box']
                szs.append(h*w)
                #szs.append(y)
            szs = np.array(szs)
            
            ### if no face: x,y,h,w = 0
            if len(szs)  == 0 :
                no_face = True
                x, y, h , w = 0,0,0,0
                x_ml, y_ml, x_mr, y_mr = 0, 0, 0, 0            
            ## else: select face
            else:
                no_face = False
                ## select bigger face
                arg_szs = np.argmax(szs)
                #arg_szs = np.argmin(szs)
                x , y , h , w = faces[arg_szs]['box']
                
                ## expand face 
            #    x = x - int(w*0.4)
            #    y = y - int(h*0.5)
            #    h = int(h*2.0)
            #    w =  int(w*1.8)
                
                ## construct mouth box
                x_ml, y_ml = faces[arg_szs]['keypoints']['mouth_left']
                x_mr, y_mr = faces[arg_szs]['keypoints']['mouth_right']
            
            if no_face:
                rectangle_face_tl_x = 0
                rectangle_face_br_x = 0
                rectangle_face_tl_y = 0
                rectangle_face_br_y = 0

                rectangle_mouth_tl_x = 0
                rectangle_mouth_br_x = 0
                rectangle_mouth_tl_y = 0
                rectangle_mouth_br_y = 0
            else:
                rectangle_face_tl_x = int(bbox[0])+x
                rectangle_face_tl_y = int(bbox[1])+y
                rectangle_face_br_x = int(bbox[0])+x+w
                rectangle_face_br_y = int(bbox[1])+y+h

            ### croppear la cara de la imagen inicial
#            crpim = frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x]
        #   crpim = np.copy(frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x])
                        
            ## construct mouth box
                rate_hight_side = 0.2
                rectangle_mouth_tl_x = int(bbox[0])+int(x_ml)
                rectangle_mouth_tl_y =  int(y_ml)+int(bbox[1])-int((int(x_mr)-int(x_ml))*rate_hight_side)
           #     rectangle_mouth_tl = (rectangle_mouth_tl_x, rectangle_mouth_tl_y)

                rectangle_mouth_br_x = int(x_mr)+int(bbox[0])
                rectangle_mouth_br_y = int(y_mr)+int(bbox[1])+int((int(x_mr)-int(x_ml))*rate_hight_side)    
            
            
            rectangle_mouth_br = (rectangle_mouth_br_x, rectangle_mouth_br_y)
            rectangle_mouth_tl = (rectangle_mouth_tl_x, rectangle_mouth_tl_y)


            #ORDER BELOW
            #frame_index: number of frame in the video
            #track_id: id of the subjet tracked
            #3-6: track frame
            #7-10: smile
            
            #### WRITE DETECTIONS
            
            list_file.write(str(frame_index)+','+
                            str(track.track_id)+','+
                            #RECTANGLE BODY
                            str(int(bbox[0])) + ','+
                            str(int(bbox[1])) + ','+
                            str(int(bbox[2])) + ','+
                            str(int(bbox[3])) + ','+
                            #RECTANGLE FACE
                            str(rectangle_face_tl_x) + ',' +
                            str(rectangle_face_tl_y) + ',' +
                            str(rectangle_face_br_x)+ ',' +
                            str(rectangle_face_br_y)+ ','+
                            #RECTANGLE MOUTH
                            str(rectangle_mouth_tl_x)+','+
                            str(rectangle_mouth_tl_y)+','+
                            str(rectangle_mouth_br_x)+','+
                            str(rectangle_mouth_br_y))
            list_file.write("\n")

            
            if Video_view or writeVideo_flag:

            ## RECTANGLE OF PERSON
                cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 2)

            # FACE RECTANGLE
                cv2.rectangle(frame, (rectangle_face_tl_x,rectangle_face_tl_y), (rectangle_face_br_x,rectangle_face_br_y),(255,255,000), 2)

            ## MOUTH RECTANGLE
                cv2.rectangle(frame, rectangle_mouth_tl,rectangle_mouth_br,(255,255,000), 2)
            # TEXT OF IDENTIFICATION
                cv2.putText(frame, str(track.track_id),(int(bbox[0]), int(bbox[1])),0, 5e-3 * 200, (0,255,0),1)
           
        if Video_view:
            for det in detections:
                bbox = det.to_tlbr()
                track_frame = frame[int(bbox[1]):int(bbox[1])+int(bbox[3]), int(bbox[0]):int(bbox[0])+int(bbox[2])]
                cv2.rectangle(frame,(int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,0,0), 2)
            
            cv2.imshow('', frame)

        if writeVideo_flag:
                # save a frame
            if not Video_view:
                for det in detections:
                    bbox = det.to_tlbr()
                    track_frame = frame[int(bbox[1]):int(bbox[1])+int(bbox[3]), int(bbox[0]):int(bbox[0])+int(bbox[2])]
                    cv2.rectangle(frame,(int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,0,0), 2)
            out.write(frame)
        
        fps  = ( fps + (1./(time.time()-t1)) ) / 2
        if print_fps:
            print("fps= %f"%(fps))
        # Press Q to stop!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video_capture.release()
    if writeVideo_flag:
         out.release()
    list_file.close()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    if len(sys.argv) >1 :
        if len(sys.argv[1]) > 0 :
            video_path = sys.argv[1]
            save_to = sys.argv[2]

            main(YOLO(), video_path, save_to, Video_view=True, nms_max_overlap= 2.0)

    else:
        main(YOLO(), None)
