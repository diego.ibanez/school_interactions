#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
from timeit import time
import warnings
import sys
#sys.path.append(".")
import cv2
import numpy as np
from PIL import Image
from deep_sort_yolov3.yolo import YOLO

from deep_sort_yolov3.deep_sort import preprocessing
from deep_sort_yolov3.deep_sort import nn_matching
from deep_sort_yolov3.deep_sort.detection import Detection
from deep_sort_yolov3.deep_sort.tracker import Tracker
from deep_sort_yolov3.tools import generate_detections as gdet
from deep_sort_yolov3.deep_sort.detection import Detection as ddet

import mtcnn
from mtcnn.mtcnn import MTCNN

warnings.filterwarnings('ignore')

#import sys
sys.path.append(".")

def main(yolo, path_video = None):

    # Deep_sort load person filter
    model_filename = 'deep_sort_yolov3/model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename,batch_size=1)


    # Definition of the parameters to Tracker
    max_cosine_distance = 0.3
    nn_budget = None
    nms_max_overlap = 1.0

    ## Init metric Tracker
    metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)

    ## Init Tracker
    tracker = Tracker(metric)

    # Gender filter
    genderModel="./gad/gender_net.caffemodel"
    genderProto="./gad/gender_deploy.prototxt"
    MODEL_MEAN_VALUES=(78.4263377603, 87.7689143744, 114.895847746)
    genderList=['Male','Female']
    genderNet=cv2.dnn.readNet(genderModel,genderProto)


    ## no path Video
    if path_video == None:
         ## Video from webcam
         video_capture = cv2.VideoCapture(0)
    else:
        ## Video from path
        video_capture = cv2.VideoCapture(path_video)

    # Define the codec and create VideoWriter object
    w = int(video_capture.get(3))
    h = int(video_capture.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    
    ## File to save video output
    out = cv2.VideoWriter('output.avi', fourcc, 5, (w, h))

    ## File to save detections
    list_file = open('detection.csv', 'w')

    ## face, mouth
    detector = MTCNN()

    ## frame init
    frame_index = -1
    ## fps init
    fps = 0.0
    while True:
        ret, frame = video_capture.read()  # frame shape 640*480*3
        if ret != True:
            #print(ret)
            #print(frame)
            break
        t1 = time.time()

        image = Image.fromarray(frame[...,::-1]) #bgr to rgb
        boxs = yolo.detect_image(image) # yolos answer   
        features = encoder(frame,boxs) # vectors encoded from the boxes of frames
    
        # Score to 1.0 here
        detections = [Detection(bbox, 1.0, feature) for bbox, feature in zip(boxs, features)]
        
        # Run non-maxima suppression
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, nms_max_overlap, scores)
        detections = [detections[i] for i in indices]
        
        # Call the tracker
        tracker.predict()
        tracker.update(detections)
        
        frame_index = frame_index +1
        for track in tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:  ##time to retrack
                continue 
            bbox = track.to_tlbr()
            #print(frame_index)
          #  crpim = frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x].copy()

            
            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 2)
           
            track_frame = frame[int(bbox[1]):int(bbox[1])+int(bbox[3]), int(bbox[0]):int(bbox[0])+int(bbox[2])]
            if len(track_frame) > 0:
                try:
                  #gray = cv2.cvtColor(track_frame, cv2.COLOR_BGR2GRAY)
                  faces = detector.detect_faces(track_frame)
                  #print(frame_index)
                except:
                  if track_frame[1] == 0:
                      print(True)
                  faces = []
            else:
                faces = []

            szs = []
            for f in faces:
                x,y,h,w = f['box']
                szs.append(h*w)
            szs = np.array(szs)
            
            ### if no face: x,y,h,w = 0
            if len(szs)  == 0 :
                x, y, h , w = 0,0,0,0
                x_ml, y_ml, x_mr, y_mr = 0, 0, 0, 0            
            ## else: select face
            else:
                ## select bigger face
                arg_szs = np.argmax(szs)
                x , y , h , w = faces[arg_szs]['box']
                
                ## expand face 
                x = x - int(w*0.4)
                y = y - int(h*0.5)
                h = int(h*2.0)
                w =  int(w*1.8)
                
                ## construct mouth box
                x_ml, y_ml = faces[arg_szs]['keypoints']['mouth_left']
                x_mr, y_mr = faces[arg_szs]['keypoints']['mouth_right']

            rectangle_face_tl_x = int(bbox[0])+x
            rectangle_face_tl_y = int(bbox[1])+y
            rectangle_face_br_x = int(bbox[0])+x+w
            rectangle_face_br_y = int(bbox[1])+y+h

            ### croppear la cara de la imagen inicial
#            crpim = frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x]
            crpim = np.copy(frame[rectangle_face_tl_y:rectangle_face_br_y, rectangle_face_tl_x:rectangle_face_br_x])
                        
            ## construct mouth box
            rate_hight_side = 0.3
            rectangle_mouth_tl_x = int(bbox[0])+int(x_ml)
            rectangle_mouth_tl_y =  int(y_ml)+int(bbox[1])+int((int(x_mr)-int(x_ml))*rate_hight_side)
            rectangle_mouth_tl = (rectangle_mouth_tl_x, rectangle_mouth_tl_y)

            rectangle_mouth_br_x = int(x_mr)+int(bbox[0])
            rectangle_mouth_br_y = int(y_mr)+int(bbox[1])-int((int(x_mr)-int(x_ml))*rate_hight_side)    
            rectangle_mouth_br = (rectangle_mouth_br_x, rectangle_mouth_br_y)

            ## RECTANGLE OF PERSON
            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,255,255), 2)

            # FACE RECTANGLE
            cv2.rectangle(frame, (rectangle_face_tl_x,rectangle_face_tl_y), (rectangle_face_br_x,rectangle_face_br_y),(255,255,000), 2)

            ## mouth rectangle
            cv2.rectangle(frame, rectangle_mouth_tl,rectangle_mouth_br,(255,255,000), 2)



            #if len(crpim) != 0:
            #    cv2.imshow('', crpim)
            #    ### GENDER IDENTIFICATION
            #    try:
            #        blob= cv2.dnn.blobFromImage(crpim, 1.0, (227,227), MODEL_MEAN_VALUES, swapRB=True)
            #        genderNet.setInput(blob)
            #        genderPreds=genderNet.forward()
            #        gender=genderList[genderPreds[0].argmax()] +' '+ str(int(genderPreds[0][genderPreds[0].argmax()]*100))

             #   except:
            #        print(crpim)
            #        print(crpim.shape)
            #        gender=''

            #else:
                #print(crpim)
                #print(crpim.shape)

            #    gender = ''

            # TEXT OF IDENTIFICATION
            cv2.putText(frame, str(track.track_id),(int(bbox[0]), int(bbox[1])),0, 5e-3 * 200, (0,255,0),1)
           
            # FACE RECTANGLE
        #cv2.rectangle(frame, (int(bbox[0])+x,int(bbox[1])+y), (int(bbox[0])+x+w,int(bbox[1])+y+h),(255,255,000), 2)

            #if writeTracks_flag:
             #   dict_ = {'frame': str(frame_index),
              #          'track_id': str(track.track_id),
              #          'x_tl_person': int(bbox[0]),
              #          'y_tl_person': int(bbox[1]),
              #          'x_br_person': int(bbox[0]) + int(bbox[2]),
              #          'y_bt_person': int(bbox[1]) + int(bbox[3]),
              #          'x_tl_face' : int(bbox[0]) + x,
              #          'y_tl_face' : int(bbox[1]) + y,
              #          'x_br_face': int(bbox[0])+x+h,
              #          'y_bt_face': int(bbox[1])+y+w,
              #          'x_tl_mouth' : int(bbox[0]) + x,
              #          'y_tl_mouth' : int(bbox[1]) + y,
              #          'x_br_mouth': int(bbox[0])+x+h,
              #          'y_bt_mouth': int(bbox[1])+y+w,
              #          'gender': gender
              #          }


            list_file.write(str(frame_index)+','+str(track.track_id)+','+str(bbox[0]) + ','+str(bbox[1]) + ','+str(bbox[2]) + ','+str(bbox[3]) + ','+ str(int(bbox[0])+x) + ',' + str(int(bbox[1])+y) + ',' + str(int(bbox[0])+x+w)+ ',' + str(int(bbox[0])+y+h))
            list_file.write("\n")

        for det in detections:
            bbox = det.to_tlbr()
            
            #track_frame = frame[int(bbox[1]):int(bbox[1])+int(bbox[3]), int(bbox[0]):int(bbox[0])+int(bbox[2])]
            #cv2.imshow('',track_frame)
            #cv2.rectangle(frame,(int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])),(255,0,0), 2)
            
        cv2.imshow('', frame)
        
        # save a frame
        out.write(frame)
        
        fps  = ( fps + (1./(time.time()-t1)) ) / 2
        print("fps= %f"%(fps))
        
        # Press Q to stop!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video_capture.release()
   # if writeVideo_flag:
    out.release()
    list_file.close()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    if len(sys.argv) >1 :
        if len(sys.argv[1]) > 0 :
            main(YOLO(), sys.argv[1])
        else:
            main(YOLO(), None)
    else:
        main(YOLO(), None)
